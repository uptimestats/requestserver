package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

var (
	TimezoneLocation *time.Location
	domainchannel    chan DomainGetResponse
	actualclient     http.Client
)

type ResponseSet struct {
	ID           int
	Name         string
	ResponseCode int
	Latency      string
	Timestamp    time.Time
	Size         int64
	Reachable    bool
	Proto        string
	Headers      map[string][]string
}

func RequestDomain(posturl string) {
	jsonbuf := new(bytes.Buffer)
	for domain := range domainchannel {
		var rs ResponseSet
		rs.ID = domain.ID
		rs.Name = domain.Url
		rs.Timestamp = time.Now().In(TimezoneLocation)
		resp, err := actualclient.Get(domain.Url)
		rs.Latency = time.Since(rs.Timestamp).String()
		if err != nil {
			rs.Reachable = false
		} else {
			rs.ResponseCode = resp.StatusCode
			rs.Size, err = io.Copy(ioutil.Discard, resp.Body)
			if err != nil {
				log.Println(err)
				rs.Size = 0
			}
			rs.Proto = resp.Proto
			rs.Headers = resp.Header
			rs.Reachable = true
			resp.Body.Close()

		}
		err = json.NewEncoder(jsonbuf).Encode(rs)
		if err != nil {
			panic(err)
		}
		presp, err := actualclient.Post(posturl, "application/json", jsonbuf)
		log.Println("server responded with", presp.StatusCode)
		if err != nil {
			log.Println(err)
		} else {
			presp.Body.Close()
		}
		jsonbuf.Reset()
	}
}

type DomainGetResponse struct {
	Url string `json:"url"`
	ID  int    `json:"id"`
}

func main() {
	configfilelocation := flag.String("f", "config.json", "Config File Location")
	flag.Parse()
	conf, err := LoadConfig(*configfilelocation)
	if err != nil {
		log.Fatal(err)
	}
	if conf.LogFileLocation != "" {
		f, err := os.OpenFile(conf.LogFileLocation, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		if err != nil {
			log.Fatalln(err)
		}
		defer f.Close()
		log.SetOutput(f)
	}

	if conf.Timezone != "" {
		TimezoneLocation, err = time.LoadLocation(conf.Timezone)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		TimezoneLocation, err = time.LoadLocation("")
		if err != nil {
			log.Fatal(err)
		}
	}

	domainchannel = make(chan DomainGetResponse, 50)
	timeout, err := time.ParseDuration(conf.GlobalTimeout)
	if err != nil {
		log.Fatalln("Error initializing global timeout:", err)
	}

	actualclient = http.Client{
		Timeout: timeout,
	}
	for i := 0; i < conf.Threads; i++ {
		go RequestDomain(conf.RemoteDomainPostURL)
	}
	for {
		res, err := actualclient.Get(conf.RemoteDomainGetURL)
		if err != nil {
			log.Println(err)
			time.Sleep(1 * time.Second)
			continue
		}

		if res.StatusCode == http.StatusNoContent {
			log.Println("no domains left, sleeping 1s")
			time.Sleep(1 * time.Second)
			continue
		}
		var geturl DomainGetResponse
		err = json.NewDecoder(res.Body).Decode(&geturl)
		if err != nil {
			log.Println(err)
			continue
		}
		res.Body.Close()

		domainchannel <- geturl
	}
}

// LoadConfig loads a json config file and returnes configuration.
func LoadConfig(path string) (*Configuration, error) {
	dat, err := ioutil.ReadFile(path)
	var config *Configuration
	if err = fromjson(string(dat), &config); err != nil {
		log.Fatal(err)
	}
	return config, err
}

type Configuration struct {
	GlobalTimeout       string
	RemoteDomainGetURL  string
	RemoteDomainPostURL string
	RemoteAuthUser      string
	RemoteAuthPass      string
	LogFileLocation     string
	Timezone            string
	Threads             int
}

func fromjson(src string, v interface{}) error {
	return json.Unmarshal([]byte(src), v)
}

func tojson(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}

func toprettyjson(v interface{}) ([]byte, error) {
	return json.MarshalIndent(v, "", "\t")
}
